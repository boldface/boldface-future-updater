# Boldface Future Updater

## Description
Create hidden future revisions that automatically turn visible at the prescribed time.

## Requirements
This plugin has been tested with WordPress 4.7. It requires PHP 5.3 or greater.

## Installation
Download the plugin zip file to your WordPress plugins directory. Extract the
files. Activate the plugin.

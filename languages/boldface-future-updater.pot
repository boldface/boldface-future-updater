#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-02-06 22:50+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco - https://localise.biz/"

#: home/nathan/boldface-future-updater/includes/bootstrap.php:39
msgid "Future Updater plugin cannot be activated."
msgstr ""

#: home/nathan/boldface-future-updater/includes/bootstrap.php:50
msgid "Cloning of this class is not allowed."
msgstr ""

#: home/nathan/boldface-future-updater/includes/bootstrap.php:180
msgid " PHP version 5.3 or greater required."
msgstr ""

#: home/nathan/boldface-future-updater/includes/bootstrap.php:198
msgid " WordPress version 4.7 or greater required."
msgstr ""

#: home/nathan/boldface-future-updater/includes/futureupdater/methods/custom-post-type.php:24
msgid "Future Revisions"
msgstr ""

#: home/nathan/boldface-future-updater/includes/futureupdater/methods/custom-post-type.php:25
msgid "Future Revision"
msgstr ""

#: home/nathan/boldface-future-updater/includes/futureupdater/methods/add-meta-boxes.php:95
msgid "Future Updater"
msgstr ""

#: home/nathan/boldface-future-updater/includes/futureupdater/methods/add-meta-boxes.php:114
msgid "Future updater date:"
msgstr ""

#: home/nathan/boldface-future-updater/includes/futureupdater/methods/load-edit.php:28
msgid "Future Updates"
msgstr ""

#. Name of the plugin
msgid "Boldface Future Updater"
msgstr ""

#. Description of the plugin
msgid "Update a post in the future."
msgstr ""

#. URI of the plugin
msgid "http://www.boldfacedesign.com/plugins/boldface-future-updater/"
msgstr ""

#. Author of the plugin
msgid "Nathan Johnson"
msgstr ""

#. Author URI of the plugin
msgid "http://www.boldfacedesign.com/author/nathan/"
msgstr ""

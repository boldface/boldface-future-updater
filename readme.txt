=== Boldface Future Updater ===
Contributors: NathanAtmoz
Requires at least: 4.7
Tested up to: 4.7.2
Stable tag: trunk
License: GPL2+
License URI: https://www.gnu.org/licenses/gpl-2.0.en.html

Create hidden future revisions that automatically turn visible at the prescribed time.

== Description ==
Create hidden future revisions that automatically turn visible at the prescribed time.

== Installation ==
Download zip file to the wp-plugins folder and extract. Activate from plugins screen.

== Changelog ==
=== 0.2 ===
* Update APIs

=== 0.1 ===
* Initial release

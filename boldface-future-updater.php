<?php
/**
 * Plugin Name: Boldface Future Updater
 * Plugin URI: http://www.boldfacedesign.com/plugins/boldface-future-updater/
 * Description: Update a post in the future.
 * Version: 0.2
 * Author: Nathan Johnson
 * Author URI: http://www.boldfacedesign.com/author/nathan/
 * Licence: GPL2+
 * Licence URI: https://www.gnu.org/licenses/gpl-2.0.en.html
 * Domain Path: /languages
 * Text Domain: boldface-future-updater
 */

//* Don't access this file directly
defined( 'ABSPATH' ) or die();

//* Start bootstraping the plugin
require( dirname( __FILE__ ) . '/includes/bootstrap.php' );
add_action( 'plugins_loaded',
  [ $bootstrap = new future_updater_bootstrap( __FILE__ ), 'register' ] );

//* Register activation and deactivation hooks
register_activation_hook( __FILE__ , [ $bootstrap, 'activation' ] );
register_deactivation_hook( __FILE__ , [ $bootstrap, 'deactivation' ] );

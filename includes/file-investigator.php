<?php

namespace Boldface;

/**
 * Looks at the file type and determines if it's an interface, class, or namespace.
 *
 * @package Boldface
 */
class file_investigator {

  /**
   * @var includes Includes file part
   *
   * @access protected
   * @since 0.2
   */
  protected $includes = '/includes';

  /**
   * @var class Class name
   *
   * @access protected
   * @since 0.2
   */
  protected $class;

  /**
   * @var filename Name of the file
   *
   * @access protected
   * @since 0.2
   */
  protected $filename;

  /**
   * @var base_path The base path of the file
   *
   * @access protected
   * @since 0.2
   */
  protected $base_path;

  /**
   * @var plugin_dir The plugin directory
   *
   * @access protected
   * @since 0.2
   */
  protected $plugin_dir;

  /**
   * Return whether the file is a theme
   *
   * @access protected
   * @since 0.2
   *
   * @return bool Whether the file is a theme
   */
  protected function is_theme() {
    isset( $this->class ) or $this->die();

    $this->filename = $this->theme_filename();
    return file_exists( $this->filename ) ? true : false;
  }

  /**
   * Return whether the file is a plugin
   *
   * @access protected
   * @since 0.2
   *
   * @return bool Whether the file is a plugin
   */
  protected function is_plugin() {
    isset( $this->class ) or $this->die();

    $this->filename = $this->plugin_filename();
    return file_exists( $this->filename ) ? true : false;
  }

  /**
   * Return the theme filename
   *
   * @access protected
   * @since 0.2
   *
   * @return string Theme filename
   */
  protected function theme_filename() {
    return $this->filename =
      get_template_directory() . $this->includes . $this->base_path;
  }

  /**
   * Return the plugin filename
   *
   * @access protected
   * @since 0.2
   *
   * @return string Plugin filename
   */
  protected function plugin_filename() {
    return $this->filename =
      WP_PLUGIN_DIR . '/' . $this->plugin_dir . $this->includes . $this->base_path;
  }

  /**
   * Return the filename
   *
   * @access public
   * @since 0.2
   *
   * @return string Filename
   */
  public function get_filename() {
    if( $this->is_theme() ) {
      return $this->theme_filename();
    }
    if( $this->is_plugin() ) {
      return $this->plugin_filename();
    }
  }

  /**
   * Set the class
   *
   * @access public
   * @since 0.2
   *
   * @return object Class instance
   */
  public function class( $class ) {
    $this->class = $class;
    $this->base_path = $this->get_file_type( $this->class );
    $this->plugin_dir = $this->get_plugin_name( $this->class );
    return $this;
  }

  /**
   * Die with error message
   *
   * @access public
   * @since 0.2
   */
  public function die() {
    wp_die(
      __( printf( 'Error attempting to load class: %1$s', $this->class ),
      'boldface-future-updater' )
    );
  }

  /**
   * Returns the path to the file
   *
   * @param string $file The incoming filename
   *
   * @access public
   * @since 0.1
   *
   * @return string The path to the file
   */
  public function get_file_type( $file ) {
    $file_parts = explode( '\\', $file );

    $path = '';

    for( $i = 1; $i < count( $file_parts ); $i++ ){
      $current_part = strtolower( $file_parts[ $i ] );
      $current_part = str_ireplace( '_', '-', $current_part );

      $path .= $this->get_file_name( $file_parts, $current_part, $i );

      if( count( $file_parts ) - 1 !== $i ){
        $path = \trailingslashit( $path );
      }
    }

    return $path;
  }


  /**
   * Returns the base directory of the plugin
   *
   * @param string $file The incoming filename
   *
   * @access public
   * @since 0.1
   *
   * @return string The base directory of the plugin
   */
  public function get_plugin_name( $file ) {
    $file_parts = preg_split( '/(?=[A-Z])/', $file );

    $path = '';

    for( $i = 1; $i < count( $file_parts ); $i++ ){
      $path .= strtolower( $file_parts[ $i ] ) . '-';
    }
    $path = str_replace( '\-', '-', $path );
    $path = explode( '\\', $path );

    return $path[0];
  }

  /**
   * Looks at the current index and returns the location of the current part
   * of the file name.
   *
   * @param array  $file_parts   All parts of the file
   * @param string $current_part Current part of the file
   * @param int    $i            The current index
   *
   * @access private
   * @since 0.1
   *
   * @return string The file name
   */
  private function get_file_name( $file_parts, $current_part, $i ) {
    $file_name  = '';

    if( count( $file_parts ) - 1 === $i ){
      if( $this->is_interface( $file_parts ) ) {
        $file_name = $this->get_interface_name( $file_parts );
      } else {
        $file_name = $this->get_class_name( $current_part );
      }
    } else {
      $file_name = $this->get_namespace_name( $current_part );
    }

    return $file_name;
  }

  /**
   * Returns whether the file is an interface
   *
   * @param array  $file_parts   All parts of the file
   *
   * @access private
   * @since 0.1
   *
   * @return bool Whether the file is an interface
   */
  private function is_interface( $file_parts ) {
    return strpos( strtolower( $file_parts[ count( $file_parts ) - 1 ] ), 'interface' );
  }

  /**
   * Return the interface name
   *
   * @param array  $file_parts   All parts of the file
   *
   * @access private
   * @since 0.1
   *
   * @return string The interface name
   */
  private function get_interface_name( $file_parts ) {
    $interface_name = explode( '_', $file_parts[ count( $file_parts ) - 1 ] );
    $interface_name = $interface_name[0];

    return "interface-$interface_name.php";
  }

  /**
   * Return the class name
   *
   * @param string $current_part The current part of the file path
   *
   * @access private
   * @since 0.1
   *
   * @return string The class name
   */
  private function get_class_name( $current_part ) {
    return "$current_part.php";
  }

  /**
   * Return the namespace name
   *
   * @param string $current_part The current part of the file path
   *
   * @access private
   * @since 0.1
   *
   * @return string The namespace name
   */
  private function get_namespace_name( $current_part ) {
    return "/$current_part";
  }
}

<?php
/**
 * Future Updater Plugin
 *
 * @package Future Updater
 */
namespace Boldface\FutureUpdater\Methods;

use \Boldface\FutureUpdater\Hooks as hooks;

defined( 'ABSPATH' ) or die();

/**
 * Class for loading the text domain
 */
class load_textdomain {

  /**
   * Object constructor
   *
   * @param string $file Main plugin file
   *
   * @access public
   * @since 0.1
   */
  public function __construct( $file ) {
    $this->file = $file;
  }

  /**
   * Load the text domain
   *
   * @access public
   * @since 0.1
   */
  public function register() {
    \load_plugin_textdomain( 'boldface-future-updater', false,
      dirname( \plugin_basename( $this->file ) ) . '/languages' );
  }
}

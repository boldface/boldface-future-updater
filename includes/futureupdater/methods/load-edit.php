<?php
/**
 * Edit API
 *
 * @package Future Updater
 */
namespace Boldface\FutureUpdater\Methods;

use \Boldface\FutureUpdater\methods as methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for interacting with columns on the posts, pages, and custom post types
 * edit screens
 */
class load_edit {

  /**
   * Add a column to posts/pages columns
   *
   * @param array $columns Column names
   *
   * @access public
   * @since 0.1
   */
  public function add_column( $columns ) {
    $columns[ 'future-update' ] = __( 'Future Updates', 'boldface-future-updater' );
    return $columns;
  }

  /**
   * Add the date to the custom column
   *
   * @param string $column_name The column name
   *
   * @access public
   * @since 0.1
   */
  public function custom_column( $column_name ) {

    $post = \get_post();
    $future_revision = new methods\future_revision( $post->ID );

    printf( $future_revision->has() ? $future_revision->datestr() : '&mdash;' );
  }

  /**
   * Return the capability required to edit the post type
   *
   * @param string $post_type The post type
   *
   * @access public
   * @since 0.2
   *
   * @return string Capability required to edit post type
   */
  public function edit_capability( $post_type ) {
    return 's' === substr( $post_type, -1 ) ?
      "edit_{$post_type}" : "edit_{$post_type}s";
  }
}

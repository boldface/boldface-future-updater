<?php
/**
 * Custom post type API
 *
 * @package Future Updater
 */
namespace Boldface\FutureUpdater\Methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for interacting with the custom post type API
 */
class custom_post_type {

  /**
   * Register a custom post type
   *
   * @access public
   * @since 0.1
   */
  public function register() {
    $labels = [
      'name'          => __( 'Future Revisions', 'boldface-future-updater' ),
      'singular_name' => __( 'Future Revision', 'boldface-future-updater' ),
    ];

    $args = [
      'labels'              => $labels,
      'exclude_from_search' => true,
      'publicly_queryable'  => false,
      'show_in_nav_menus'   => true,
      'show_in_menu'        => true,
      'show_in_admin_bar'   => false,
      'show_ui'             => true,
      'map_meta_cap'        => true,
      'capability_type'     => 'future-revision',
      'supports'            => [ 'title', 'editor', 'revisions' ],
    ];

    \register_post_type( $post_type = 'future-revisions', $args );
  }
}

<?php
/**
 * Validator API
 *
 * @package Future Updater
 */
namespace Boldface\FutureUpdater\Methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for
 */
class post_validator {

  /**
   * @var Main plugin file
   *
   * @access protected
   * @since 0.1
   */
  protected $file;

  /**
   * @var int UNIX timestamp for future update
   *
   * @access public
   * @since 0.1
   */
  protected $timestamp;

  /**
   * Object constructor
   *
   * @param string $file Main plugin file
   *
   * @access public
   * @since 0.1
   */
  public function __construct( $file ) {
    $this->file = $file;
    $this->timestamp = $this->timestamp();
  }

  /**
   * Validate the post prior to saving to the database
   *
   * @access public
   * @since 0.1
   *
   * @return bool Whether the post validates
   */
  public function content_save_pre( $post ) {
    if( ! isset( $post ) ) {
      return false;
    }

    //* Return false if this is saved from a draft
    if( 'draft' === $post->post_status ) {
      return false;
    }

    //* Return false if timestamp is in the past
    if( ( $this->timestamp - time() ) < 0 ) {
      return false;
    }

    $this->future_content = $post->content;
    return true;
  }

  /**
   * Make sure the user can update the post
   *
   * @param int     $post_id     Post ID
   * @param WP_Post $post_after  Post object after update
   * @param WP_Post $post_before ost object after update
   *
   * @access public
   * @since 0.1
   *
   * @return bool Whether the user can update the post
   */
  public function post_updated( $post_id, $post_after, $post_before ) {

    //* Return early if doing an autosave
    if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
      return false;
    }

    //* Return early if the post wasn't already published
    $original_post_status = isset( $_POST[ 'original_post_status' ] ) ?
      \esc_attr( $_POST[ 'original_post_status' ] ) : '';

    //* Return early if the original post status isn't publish or pending
    if( isset( $original_post_status ) ) {
      if( 'publish' !== $original_post_status && 'pending' !== $original_post_status ) {
        return false;
      }
    }

    //* Return early if not a valid nonce
    if( ! \check_admin_referer( \plugin_basename( $this->file ), 'future-updater-nonce' ) ) {
      return false;
    }

    //* Return early if the user can't edit the post type
    $post_type = isset( $_POST[ 'post_type' ] ) ? \esc_attr( $_POST[ 'post_type' ] ) : '';
    if( ! isset( $post_type ) || ! \current_user_can( "edit_$post_type", $id ) ) {
      return false;
    }

    //* Return early if timestamp isn't set or it's in the past
    if( false === $this->timestamp || $this->timestamp < time() ) {
      return false;
    }

    return true;
  }

  /**
   * Return the timestamp sent in the post request
   *
   * @access public
   * @since 0.2
   *
   * @return int|false The timestamp sent in the post request
   */
  public function timestamp() {
    return isset( $this->timestamp ) ? $this->timestamp :
      $this->timestamp = isset( $_POST[ 'future_update' ] ) ?
        strtotime( \esc_attr( $_POST[ 'future_update' ] ) ) : false;
  }
}

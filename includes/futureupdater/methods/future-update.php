<?php
/**
 * Future Update API
 *
 * @package Future Updater
 */
namespace Boldface\FutureUpdater\Methods;

use \Boldface\FutureUpdater\methods as methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for
 */
class future_update {

  /**
   * @var Main plugin file
   *
   * @access protected
   * @since 0.1
   */
  protected $file;

  /**
   * @var Instance of the post_validator object
   *
   * @access protected
   * @since 0.1
   */
  protected $post_validator;

  /**
   * @var string The future content
   *
   * @access protected
   * @since 0.1
   */
  protected $future_content;

  /**
   * @var int UNIX timestamp
   *
   * @access protected
   * @since 0.1
   */
  protected $timestamp;

  /**
   * Object constructor
   *
   * @param string $file Main plugin file
   *
   * @access public
   * @since 0.1
   */
  public function __construct( $file ) {
    $this->file = $file;
    $this->post_validator = new methods\post_validator( $file );
    $this->timestamp = $this->post_validator->timestamp();
  }

  /**
   * Maybe modify the content before saving to the database
   *
   * @param string $content The post content
   *
   * @access public
   * @since 0.1
   *
   * @return string The modified post content
   */
  public function content_save_pre( $content ) {
    //* Return early if the post content doesn't validate
    $post = \get_post();
    if( ! $this->post_validator->content_save_pre( $post ) ) {
      return $content;
    }

    //* If we have already set $this->future content, return it
    //* This prevents infinite looping
    if( isset( $this->future_content ) && ! empty( $this->future_content ) ) {
      return $this->future_content;
    }

    if( 'future-revisions' === $post->post_type ) {
      return $content;
    }

    //* Set $this->future_content to use later
    $this->future_content = $content;

    //* Return last post content
    return $post->post_content;
  }

  /**
   * Make sure the user can update the post, then save the meta data
   *
   * @param int     $post_id     Post ID
   * @param WP_Post $post_after  Post object after update
   * @param WP_Post $post_before Post object after update
   *
   * @access public
   * @since 0.1
   */
  public function post_updated( $post_id, $post_after, $post_before ) {
    if( ! $this->post_validator->post_updated( $post_id, $post_after, $post_before ) ) {
      return;
    }

    if( 'future-revisions' === $post_before->post_type ) {
      $meta = \future_revision_post_meta( $post_before->ID );
      $get = $meta->get();

      //* If the new timestamp isn't equal to the present timestamp, update it
      $latest = $meta->latest();
      if( $meta->has( $latest ) && $this->timestamp !== $latest ) {
        $meta->add( $this->timestamp, $meta->value( $latest ) );
        $meta->remove( $latest );
        $id = ( $meta->value( $this->timestamp ) )[ 'id' ];
        $orig = \future_revision_post_meta( $id );
        $orig->add( $this->timestamp, $meta->value( $latest ) );
        $orig->remove( $latest );
      }
      return;
    }

    $future_revision = $post_after;
    $future_revision->post_type = 'future-revisions';
    $future_revision->post_content = \wp_unslash( $this->future_content );

    if( is_integer( $post_id ) && is_integer( $this->timestamp ) ) {
      $this->set_future_update( $post_id, $this->timestamp, $future_revision );
    }
  }

  /**
   * Wrapper to set future-revision
   *
   * @param int     $id        The post ID
   * @param int     $timestamp Unix timestamp of future revision expiration
   * @param WP_Post $post      The future revision
   *
   * @access protected
   * @since 0.1
   */
  protected function set_future_update( $id, $timestamp, $post ) {

    $future_id = $this->save( $timestamp, $post );

    $meta = \future_revision_post_meta( $id );
    $meta->add( $timestamp, [ 'id' => $id, 'future_id' => $future_id, ] );

    $meta = \future_revision_post_meta( $future_id );
    $meta->add( $timestamp, [ 'id' => $id, 'future_id' => $future_id, ] );
  }

  /**
   * Save the future revision
   *
   * @param int     $timestamp Expiration timestamp
   * @param WP_Post $post      The WordPress post
   *
   * @access protected
   * @since 0.1
   *
   * @return int ID of inserted future revision
   */
  protected function save( $timestamp, $post ) {
    //* Check to see if this future-revision already exists at this timestamp
    $query = new \WP_Query( [
      'post_type' => 'future-revisions',
      'name'      => $post->post_name . '-' . $timestamp,
    ] );

    //* Get the ID
    if( count( $query->posts ) > 0 ) {
      foreach( $query->posts as $post ) {
        if( isset( $post->ID ) ) {
          $id = $post->ID;
          break;
        }
      }
    }

    $future_revision = new methods\future_revision( $post->ID );
    if( $future_revision->has( $timestamp ) ) {
      $revision = $future_revision->get( $timestamp );
      $id = $revision->ID;
    }

    $future_post = [
      'ID'            => ( isset( $id ) && ! empty( $id ) ? $id : false ),
      'post_author'   => $post->post_author,
      'post_date'     => $post->post_date,
      'post_date_gmt' => $post->post_date_gmt,
      'post_content'  => \wp_unslash( $this->future_content ),
      'post_title'    => $post->post_title,
      'post_excerpt'  => $post->post_excerpt,
      'post_status'   => 'private',
      'post_name'     => ( isset( $id ) && ! empty( $id ) ? $post->post_name : $post->post_name. ' - ' . $timestamp ),
      'post_type'     => 'future-revisions',
    ];

    return $future_id = \wp_insert_post( $future_post );
  }
}

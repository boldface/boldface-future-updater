<?php
/**
 * Methods/post
 *
 * @package Future Updater
 */
namespace Boldface\FutureUpdater\Methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for adding hooks to the WordPress post
 */
class the_post {

  /**
   * Object constructor
   *
   * @param int    $id              Post ID
   * @param object $future_revision Future revision object
   *
   * @access public
   * @since 0.2
   */
  public function __construct( $id, $future_revision ) {
    $this->id = $id;
    $this->future_revision = $future_revision;
  }

  /**
   * To delete the expired post, we'll first transfer the post content and post
   * meta to the current post as a new revision, and then delete the future
   * revision.
   *
   * @access public
   * @since 0.2
   */
  public function delete() {
    $expired = $this->future_revision->get_expired();
    \wp_update_post( [
      'ID'           => $this->id,
      'post_title'   => $expired->post_title,
      'post_content' => $expired->post_content,
    ] );
    $this->future_revision->delete( $this->future_revision->expiration() );
  }
}

<?php
/**
 * Post Meta API
 *
 * @package Future Updater
 */
namespace Boldface\FutureUpdater\Methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for interacting with the WordPress Post Meta API
 */
class post_meta {

  /**
   * @var Post ID
   *
   * @access protected
   * @since 0.1
   */
  protected $id;

  /**
   * @var Meta data key
   *
   * @access protected
   * @since 0.1
   */
  protected $key = '';

  /**
   * @var Meta data value
   *
   * @access protected
   * @since 0.1
   */
  protected $value;

  /**
   * Constructor
   *
   * @param int $id Post ID
   *
   * @access public
   * @since 0.1
   */
  public function __construct( $id ) {
    $this->id = $id;
    $this->post_meta = \maybe_unserialize( \get_post_meta( $id ) );
  }

  /**
   * Whether the key exists
   *
   * @param string $key Name of the key
   *
   * @access public
   * @since 0.1
   *
   * @return bool Whether the key exists
   */
  public function has( $key ) {
    return false === $this->value() ? false : true;
  }

  /**
   * Set the key property
   *
   * @param string $key Name of the key
   *
   * @access public
   * @since 0.1
   *
   * @return object $this object
   */
  public function key( $key ) {
    $this->key = $key;
    return $this;
  }

  /**
   * Return the value of a key
   *
   * @param string $key Name of the key
   *
   * @access public
   * @since 0.1
   *
   * @return mixed|bool Value of key or false
   */
  public function value( $key = '' ) {
    $this->key( $key );
    return isset( $this->value ) ? $this->value : $this->get_post_meta();
  }

  /**
   * Wrapper to update post meta
   *
   * @access protected
   * @since 0.1
   *
   * @return bool Whether the post meta was successfully updated
   */
  protected function update_post_meta() {
    return isset( $this->id ) && isset( $this->key ) ?
      \update_post_meta( $this->id, $this->key, \maybe_serialize( $this->value ) ) :
      false;
  }

  /**
   * Wrapper to get post meta
   *
   * @access protected
   * @since 0.1
   *
   * @return mixed|bool The post meta or false
   */
  protected function get_post_meta() {
    return \maybe_unserialize( \get_post_meta(
      $this->id, isset( $this->key ) ? $this->key : '', true ) );
  }
}

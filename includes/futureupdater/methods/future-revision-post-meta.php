<?php
/**
 * Future Revision Post Meta API
 *
 * @package Future Updater
 */
namespace Boldface\FutureUpdater\Methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for interacting with the Future Revision Post Meta API
 */
class future_revision_post_meta {

  /**
   * @var Post ID
   *
   * @access protected
   * @since 0.1
   */
  protected $id;

  /**
   * @var Meta data key
   *
   * @access protected
   * @since 0.1
   */
  protected $key = '_future_updater';

  /**
   * @var Meta data value
   *
   * @access protected
   * @since 0.1
   */
  protected $value;

  /**
   * Constructor
   *
   * @param int $id Post ID
   *
   * @access public
   * @since 0.1
   */
  public function __construct( $id ) {
    $this->id = $id;
    $this->value = $this->get();
  }

  /**
   * Return the value of a key
   *
   * @param string $key Name of the key
   *
   * @access public
   * @since 0.1
   *
   * @return Value of key
   */
  public function value( $key ) {
    if( $this->has( $key ) ) {
      return $this->value[ $key ];
    }
  }

  /**
   * Whether the key exists
   *
   * @param string $key Name of the key
   *
   * @access public
   * @since 0.1
   *
   * @return bool Whether the key exists
   */
  public function has( $key ) {
    if( isset( $this->value[ $key ] ) ) {
      return true;
    }
    return false;
  }

  /**
   * Add a key value pair
   *
   * @param string $key   Name of the key
   * @param mixed  $value Value
   *
   * @access public
   * @since 0.1
   */
  public function add( $key, $value ) {
    $this->value[ $key ] = $value;
    $this->update_post_meta();
  }

  /**
   * Remove a key value pair
   *
   * @param string $key Name of the key
   *
   * @access public
   * @since 0.1
   */
  public function remove( $key ) {
    unset( $this->value[ $key ] );
    $this->update_post_meta();
  }

  /**
   * Return the keys
   *
   * @access public
   * @since 0.2
   *
   * @return array Array of keys
   */
  public function keys() {
    return array_keys( $this->value );
  }

  /**
   * Update post meta
   *
   * @access protected
   * @since 0.1
   */
  protected function update_post_meta() {
    \update_post_meta( $this->id, $this->key, \maybe_serialize( $this->value ) );
  }

  /**
   * Get all the post meta
   *
   * @access public
   * @since 0.1
   */
  public function get() {
    $value = \maybe_unserialize( \get_post_meta( $this->id, $this->key, true ) );
    return is_array( $value ) ? $value : [];
  }

  /**
   * Return the number of post meta elements
   *
   * @access public
   * @since 0.1
   *
   * @return int Number of post meta eletments
   */
  public function count() {
    return (int) count( $this->value );
  }

  /**
   * Return the latest key
   *
   * @access public
   * @since 0.1
   *
   * @return The latest key
   */
  public function latest() {
    if( ! is_array( $this->value ) ) return false;
    $this->sort();
    reset( $this->value );
    return key( $this->value );
  }

  /**
   * Return the sorted array
   *
   * @param string $order Whether to sort into ascending or descending order
   *
   * @access public
   * @since 0.1
   *
   * @return array|bool Return the sorted array or false if not sortable
   */
  public function sort( $order = 'ASC' ) {
    return is_array( $this->value ) ?
      strtoupper( $order === 'DESC' ) ? krsort( $this->value ): ksort( $this->value ) :
      false;
  }

  /**
   * Deletes all future revision post meta
   *
   * @access public
   * @since 0.2
   */
  public function delete() {
    \delete_post_meta( $this->id, $this->key );
  }
}

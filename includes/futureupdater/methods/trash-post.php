<?php
/**
 * Methods/trash_post
 *
 * @package Future Updater
 */
namespace Boldface\FutureUpdater\Methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for deleting the WordPress post
 */
class trash_post {

  /**
   * @var string $post_type The post type
   *
   * @access protected
   * @since 0.1
   */
  protected $post_type;

  /**
   * Object constructor
   *
   * @access public
   * @since 0.2
   */
  public function __construct( $post_type ) {
    $this->post_type = $post_type;
  }

  /**
   * Fired after a post is trashed.
   *
   * @param int $id Post ID
   *
   * @access public
   * @since 0.2
   */
  public function trashed_post( $id ) {
    if( 'future-revisions' !== $this->post_type ) {
      return;
    }

    $meta = \future_revision_post_meta( $id )->get();
    $timestamps = \future_revision_post_meta( $id )->keys();

    if( 0 === count( $timestamps ) ) {
      return;
    }

    foreach( $timestamps as $timestamp ) {
      if( $meta[ $timestamp ][ 'future_id' ]  === $id ) {
        \future_revision_post_meta( $meta[ $timestamp ][ 'id' ] )->remove( $timestamp );
      }
    }
  }

  /**
   * Fired after a post is untrashed.
   *
   * @param int $id Post ID
   *
   * @access public
   * @since 0.2
   */
  public function untrashed_post( $id ) {
    if( 'future-revisions' !== $this->post_type ) {
      return;
    }

    $meta = \future_revision_post_meta( $id )->get();
    $timestamps = \future_revision_post_meta( $id )->keys();

    if( 0 === count( $timestamps ) ) {
      return;
    }

    foreach( $timestamps as $timestamp ) {
      \future_revision_post_meta( $meta[ $timestamp ][ 'id' ] )->add( $timestamp, $val );
    }
  }

  /**
   * Fired after a post is deleted.
   *
   * @param int $id Post ID
   *
   * @access public
   * @since 0.2
   */
  public function deleted_post( $id ) {
    if( 'future-revisions' === $this->post_type ) {
      return;
    }

    $post = \get_post( $id );

    $args = [
      'post_type' => 'future-revisions',
      's'         => $post->post_title,
    ];
    $query = new \WP_Query( $args );
    if( (int) $query->found_posts === 0 ) {
      return;
    }

    foreach( $query->posts as $post ) {
      $meta = \future_revision_post_meta( $post->ID )->get();
      foreach( $meta as $timestamp => $ids ) {
        if( $ids[ 'id' ] === $id - 1 ) {
          \wp_delete_post( $ids[ 'future_id' ], true );
        }
      }
    }
  }
}

<?php
/**
 * Future Revision API
 *
 * @package Future Updater
 */
namespace Boldface\FutureUpdater\Methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for interacting with Future Revisions
 */
class future_revision {

  /**
   * @var Future revision post meta
   *
   * @access protected
   * @since 0.1
   */
  protected $post_meta;

  /**
   * The WP_Query
   *
   * @access protected
   * @since 0.1
   */
  protected $query;

  /**
   * Constructor
   *
   * @param int $id        Post ID
   * @param int $timestamp UNIX timestamp
   *
   * @access public
   * @since 0.1
   */
  public function __construct( $id = 0, $timestamp = 0 ) {

    if( ! is_integer( $id ) || 0 === $id ) {
      wp_die( "$id is not a valid post ID." );
    }

    //* Attempt to get the post
    $post = \get_post( $id );

    //* Return early if the WP_Post isn't set for this ID
    if( ! isset( $post ) ) {
      return;
    }

    $this->post_meta = \future_revision_post_meta( $id );
    $metas = $this->post_meta->get();
    foreach( $metas as $timestamp => $meta ) {
      if( null === \get_post( $meta[ 'future_id' ] ) ) {
        $this->post_meta->remove( $timestamp );
      }
    }
    $this->timestamp = $timestamp;
  }

  /**
   * Return whether the query has any future revision posts.
   *
   * If optional $date parameter is set, return whether there
   * is a future revision at that timestamp.
   *
   * @param int $date UNIX timestamp (optional)
   *
   * @access public
   * @since 0.1
   *
   * @return bool Whether the post has future revision
   */
  public function has( $timestamp = 0 ) {
    if( $this->is_valid_timestamp( $timestamp ) ) {
      return $this->post_meta->has( $timestamp );
    }
    return $this->post_meta->count() >= 1 ? true : false;
  }

  /**
   * Return whether the post has expired future revision
   *
   * If optional $date parameter is set, return whether there
   * is an expired future revision at that timestamp.
   *
   * @param int $date UNIX timestamp (optional)
   *
   * @access public
   * @since 0.1
   *
   * @return bool Whether the post has expired future revisions
   */
  public function has_expired( $timestamp = 0 ) {
    return $this->has( $timestamp ) && ( $this->expiration( $timestamp ) <= time() ) ?
      true : false;
  }

  /**
   * Get the posts future revisions
   *
   * If optional $date parameter is set, return the future revision
   * at that timestamp.
   *
   * @param int $date UNIX timestamp (optional)
   *
   * @access public
   * @since 0.1
   *
   * @return
   */
  public function get( $timestamp = 0 ) {
    $post_meta = $this->post_meta->value( $timestamp );
    return \get_post( $post_meta[ 'future_id' ] );
  }

  /**
   * Return the expired future revisions
   *
   * If optional $date parameter is set, return the expired future revision
   * at that timestamp.
   *
   * @param int $date UNIX timestamp (optional)
   *
   * @access public
   * @since 0.1
   *
   * @return \WP_Post The post has expired future revision
   */
  public function get_expired( $timestamp = 0 ) {
    $expired = $this->post_meta->value( $this->expiration( $timestamp ) );
    return \get_post( $expired[ 'future_id' ] );
  }

  /**
   * Delete the future revision
   *
   * @param int $id Post ID
   *
   * @access public
   * @since 0.1
   */
  public function delete( $timestamp = 0 ) {
    if( ! $this->is_valid_timestamp( $timestamp ) ) {
      return;
    }
    $all = $this->post_meta->get();
    if( isset( $all[ $timestamp ] ) ) {
      $id = $all[ $timestamp ][ 'future_id' ];
      $this->post_meta->remove( $timestamp );
      \wp_delete_post( $id );
    }
  }

  /**
   * Return when the future revision expires
   *
   * @access public
   * @since 0.1
   *
   * @return int UNIX timestamp of the expiration
   */
  public function expiration( $timestamp = 0 ) {
    if( $this->is_valid_timestamp( $timestamp ) ) {
      return $timestamp;
    }
    return $this->post_meta->latest();
  }

  protected function is_valid_timestamp( $timestamp = 0 ) {
    if( isset( $timestamp ) && is_integer( $timestamp ) && 0 !== $timestamp ) {
      $this->timestamp( $timestamp );
      return true;
    }
    return false;
  }

  /**
   * Set the timestamp
   *
   * @param int $timestamp UNIX timestamp
   *
   * @access public
   * @since 0.1
   *
   * @return $this Object instance
   */
  public function timestamp( $timestamp ) {
    $this->timestamp = (int) $timestamp;
    return $this;
  }

  /**
   * See if the date is expired
   *
   * @param int $date UNIX timestamp
   *
   * @access public
   * @since 0.1
   *
   * @return bool Whether the date is expired
   */
  public function is_expired( $date ) {
    return ( intval( $date ) - strtotime( 'now' ) ) <= 0 ? true : false;
  }

  /**
   * Get the future updater date as a string
   *
   * @param string $format Date format
   *
   * @access public
   * @since 0.1
   *
   * @return string The time as a string
   */
  public function datestr( $format = 'F j, Y h:i:s A' ) {
    $date = $this->expiration();
    return isset( $date ) && 0 !== $date ?
      \get_date_from_gmt( date( 'Y-m-d H:i:s', $date ), $format ) : '';
  }
}

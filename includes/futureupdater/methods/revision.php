<?php
/**
 * Revision API
 *
 * @package Future Updater
 */
namespace Boldface\FutureUpdater\Methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for interacting with WordPress revisions
 */
class revision {
  /**
   * Object constructor
   *
   * @param int $id WP_Post ID
   *
   * @access public
   * @since 0.1
   */
  public function __construct( $id ) {
    $this->id = $id;
  }

  /**
   * Get the last revision
   * This section of code is slightly modified from WordPress core
   *
   * @access public
   * @since 0.1
   *
   * @return WP_Post The last revision
   */
  public function get_last_revision() {
    //* Be sure to check has_last_revision() before calling this function
    $revisions = \wp_get_post_revisions( $this->id );

    foreach( $revisions as $revision ) {
      if( false !== strpos( $revision->post_name, "{$revision->post_parent}-revision" ) )  {
        return $revision;
      }
    }
  }

  /**
   * Helper function to determine if the post has a past revision
   *
   * @access public
   * @since 0.1
   *
   * @return bool Whether the post has a past revision.
   */
  public function has_last_revision() {
    //* If there are no revisions, wp_get_post_revisions() returns an empty array
    if( [] === \wp_get_post_revisions( $this->id ) ){
      return false;
    }
    return true;
  }
}

<?php
/**
 * Meta Box API
 *
 * @package Future Updater
 */
namespace Boldface\FutureUpdater\Methods;

use \Boldface\FutureUpdater\methods as methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for interacting the WordPress Meta Box API
 */
class add_meta_boxes {

  /**
   * @var Main plugin file
   *
   * @access protected
   * @since 0.1
   */
  protected $file;

  /**
   * @var Datepicker CSS URL
   *
   * @access protected
   * @since 0.2
   */
  protected $datepicker_css_url =
    'https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/themes/smoothness/jquery-ui.css';

  /**
   * Object constructor
   *
   * @param string $file Main plugin file
   *
   * @access public
   * @since 0.1
   */
  public function __construct( $file ) {
    $this->file = $file;
  }

  /**
   * Enqueue datepicker and timepicker scripts and styles
   *
   * @access public
   * @since 0.1
   */
  public function admin_enqueue_scripts() {
    \wp_enqueue_script( 'jquery-ui-datepicker' );
    \wp_enqueue_script( 'jquery-ui-slider', [ 'jquery-ui-datepicker '] );
    \wp_enqueue_script(
      'jquery-ui-timepicker',
      \plugins_url( '/js/jquery-ui-timepicker-addon.js', $this->file ),
      [ 'jquery-ui-datepicker', 'jquery-ui-slider']
    );

    \wp_enqueue_style( 'jquery-ui-datepicker', $this->datepicker_css_url );
    \wp_enqueue_style(
      'jquery-ui-timepicker',
      \plugins_url( '/css/jquery-ui-timepicker-addon.css', $this->file )
    );
  }

  /**
   * Print the timepicker script
   *
   * @access public
   * @since 0.1
   */
  public function admin_print_footer_scripts() {
    printf( '
    <script type="text/javascript">
      jQuery(document).ready(function($){
        $("#datepicker").datetimepicker({
          timeFormat: \'hh:mm tt z\'
        });
      });
    </script>');
  }

  /**
   * Add a meta box
   *
   * @access public
   * @since 0.1
   */
  public function add_meta_box() {
    \add_meta_box(
      'future_updater',
      __( 'Future Updater', 'boldface-future-updater' ),
      [ $this, 'the_date_picker' ],
      null,
      'side',
      'high'
    );
  }

  /**
   * Print the date picker
   *
   * @param \WP_Post $post A WP_Post object
   *
   * @access public
   * @since 0.1
   */
  public function the_date_picker( $post ) {
    \wp_nonce_field( \plugin_basename( $this->file ), 'future-updater-nonce' );
    printf( '<span class="screen-reader-text">%1$s</span>',
      __( 'Future updater date:', 'boldface-future-updater' ) );
    printf( '<input type="text" id="datepicker" name="future_update" value="%1$s"/>',
      ( new methods\future_revision( (int) $post->ID ) )->datestr() );
  }
}

<?php
/**
 * Hooks/meta_box
 *
 * @package Future Updater
 */
namespace Boldface\FutureUpdater\Hooks;

use \Boldface\FutureUpdater\methods as methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for adding hooks to interact with the WordPress Meta Box API
 */
class add_meta_boxes {

  /**
   * @var object Meta box methods
   *
   * @access protected
   * @since 0.1
   */
  protected $methods;

  /**
   * Object constructor
   *
   * @param string $file Main plugin file
   *
   * @access public
   * @since 0.1
   */
  public function __construct( $file ) {
    $this->methods = new methods\add_meta_boxes( $file );
  }

  /**
   * Add actions to add css, js, and meta box
   *
   * @access public
   * @since 0.1
   */
  public function register() {
    \add_action( 'admin_enqueue_scripts',
      [ $this->methods, 'admin_enqueue_scripts' ] );

    \add_action( 'admin_print_footer_scripts',
      [ $this->methods, 'admin_print_footer_scripts' ] );

    \add_action( 'add_meta_boxes',
      [ $this->methods, 'add_meta_box' ] );
  }
}

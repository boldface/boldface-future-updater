<?php
/**
 * Hooks/current_screen
 *
 * @package Future Updater
 */
namespace Boldface\FutureUpdater\Hooks;

use \Boldface\FutureUpdater\hooks as hooks;

defined( 'ABSPATH' ) or die();

/**
 * Class for adding hooks to the WordPress current_screen
 */
class current_screen {

  /**
   * Use the current screen to get the post_type. And add hooks.
   *
   * @access public
   * @since 0.2
   */
  public function register() {

    $post_type = ( get_current_screen() )->post_type;

    \add_action( 'load-edit.php',
      [ new hooks\load_edit( $post_type ), 'register' ] );

    \add_action( 'current_screen',
      [ new hooks\trash_post( $post_type ), 'register' ], 15 );
  }
}

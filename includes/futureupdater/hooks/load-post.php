<?php
/**
 * Hooks\load_post
 *
 * @package Future Updater
 */
namespace Boldface\FutureUpdater\Hooks;

use \Boldface\FutureUpdater\hooks as hooks;
use \Boldface\FutureUpdater\methods as methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for adding hooks to the load post screens
 */
class load_post {

  /**
   * @var Main plugin file
   *
   * @access protected
   * @since 0.2
   */
  protected $file;

  /**
   * Object constructor
   *
   * @access public
   * @since 0.2
   */
  public function __construct( $file ) {
    $this->file = $file;
  }

  /**
   * Add action to add meta boxes
   *
   * @access public
   * @since 0.2
   */
  public function register() {
    \add_action( 'add_meta_boxes',
      [ new hooks\add_meta_boxes( $this->file ), 'register' ], 5 );
  }
}

<?php
/**
 * Hooks/post
 *
 * @package Future Updater
 */
namespace Boldface\FutureUpdater\Hooks;

use \Boldface\FutureUpdater\methods as methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for adding hooks to the WordPress post
 */
class the_post {

  /**
   * @var object future_revision
   *
   * @access protected
   * @since 0.1
   */
  protected $future_revision;

  /**
   * If there are expired future revisions for this post, add filter and action
   * to temporarily use the expired content, and deleted the expired content
   *
   * @param \WP_Post  $post  The WordPress post item
   * @param \WP_Query $query The WordPress query item
   *
   * @access public
   * @since 0.1
   */
  public function the_post( $post, $query ) {
    $this->future_revision = new methods\future_revision( $post->ID );

    if( $this->future_revision->has_expired() ) {
      $this->expired = $this->future_revision->get_expired();
      \add_filter( 'the_title', [ $this, 'the_title' ] );
      \add_filter( 'the_content', [ $this, 'the_content' ] );
      \add_action( 'shutdown',
        [ new methods\the_post( $post->ID, $this->future_revision ), 'delete' ] );
    }
  }

  /**
   * Return the expired future revision post title
   *
   * @param string $title The post title
   *
   * @access public
   * @since 0.1
   *
   * @return string The expired future revision post title
   */
  public function the_title( $title ) {
    \remove_filter( 'the_title', [ $this, 'the_title' ] );
    return $this->expired->post_title;
  }

  /**
   * Return the expired future revision post content
   *
   * @param string $content The post content
   *
   * @access public
   * @since 0.1
   *
   * @return string The expired future revision post content
   */
  public function the_content( $content ) {
    \remove_filter( 'the_content', [ $this, 'the_content' ] );
    return $this->expired->post_content;
  }
}

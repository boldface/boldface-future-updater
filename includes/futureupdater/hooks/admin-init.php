<?php
/**
 * Hooks/admin_init
 *
 * @package Future Updater
 */
namespace Boldface\FutureUpdater\Hooks;

use \Boldface\FutureUpdater\hooks as hooks;

defined( 'ABSPATH' ) or die();

/**
 * Class for adding hooks to the WordPress admin initiation
 */
class admin_init {

  /**
   * @var Main plugin file
   *
   * @access protected
   * @since 0.1
   */
  protected $file;

  /**
   * Object constructor
   *
   * @param string $file Main plugin file
   *
   * @access public
   * @since 0.1
   */
  public function __construct( $file ) {
    $this->file = $file;
  }

  /**
   * Add several actions to load the admin.
   *
   * @access public
   * @since 0.1
   */
  public function register() {
    //* Fires on load-post.php to register hooks required on load post page
    \add_action( 'load-post.php', [ new hooks\load_post( $this->file ), 'register' ] );

    //* Fires on current_screen to modify the load-edit screen
    \add_action( 'current_screen', [ new hooks\current_screen(), 'register' ] );

    //* Fires on admin_init to look for future updates
    \add_action( 'admin_init', [ new hooks\future_update( $this->file ), 'register' ] );
  }
}

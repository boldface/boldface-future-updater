<?php
/**
 * Hooks/trash_post
 *
 * @package Future Updater
 */
namespace Boldface\FutureUpdater\Hooks;

use \Boldface\FutureUpdater\hooks as hooks;
use \Boldface\FutureUpdater\methods as methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for adding hooks to handle delete posts
 */
class trash_post {

  /**
   * @var object Meta box methods
   *
   * @access protected
   * @since 0.1
   */
  protected $methods;

  /**
   * Object constructor
   *
   * @access public
   * @since 0.2
   */
  public function __construct( $post_type ) {
    $this->methods = new methods\trash_post( $post_type );
  }

  /**
   * Add several actions to handle trashed, untrashed, and deleted posts.
   *
   * @param int $id Post ID
   *
   * @access public
   * @since 0.2
   */
  public function register( $id ) {
    \add_action( 'trashed_post', [ $this->methods, 'trashed_post' ], 10, 1 );
    \add_action( 'untrashed_post', [ $this->methods, 'untrashed_post' ], 10, 1 );
    \add_action( 'deleted_post', [ $this->methods, 'deleted_post' ], 10, 1 );
  }
}

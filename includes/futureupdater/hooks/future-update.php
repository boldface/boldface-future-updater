<?php
/**
 * Hooks\future_update
 *
 * @package Future Updater
 */
namespace Boldface\FutureUpdater\Hooks;

use \Boldface\FutureUpdater\methods as methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for adding hooks to interact with future updates
 */
class future_update {

  /**
   * Object constructor
   *
   * @param string $file Main plugin file
   *
   * @access public
   * @since 0.1
   */
  public function __construct( $file ) {
    $this->future_update = new methods\future_update( $file );
  }

  /**
   * Add filters to see if there are future updates
   *
   * @access public
   * @since 0.1
   */
  public function register() {
    //* Fires before the content is saved to the database
    \add_filter( 'content_save_pre',
      [ $this->future_update, 'content_save_pre' ], 10, 1 );

    //* Fires after the content is saved to the database
    \add_action( 'post_updated',
      [ $this->future_update, 'post_updated' ], 10, 3 );
  }
}

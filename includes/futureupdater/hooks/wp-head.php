<?php
/**
 * Hooks/wp
 *
 * @package Future Updater
 */
namespace Boldface\FutureUpdater\Hooks;

use \Boldface\FutureUpdater\hooks as hooks;

defined( 'ABSPATH' ) or die();

/**
 * Class for adding hooks to the WordPress public initiation
 */
class wp_head {

  /**
   * Add action that fires on the_post.
   *
   * @access public
   * @since 0.1
   */
  public function register() {
    \add_action( 'the_post', [ new hooks\the_post(), 'the_post' ], 10, 2 );
  }
}

<?php
/**
 * Hooks\load_edit
 *
 * @package Future Updater
 */
namespace Boldface\FutureUpdater\Hooks;

use \Boldface\FutureUpdater\hooks as hooks;
use \Boldface\FutureUpdater\methods as methods;

defined( 'ABSPATH' ) or die();

/**
 * Class for adding hooks to the post edit screens
 */
class load_edit {

  /**
   * @var string $post_type The post type
   *
   * @access protected
   * @since 0.1
   */
  protected $post_type;

  /**
   * @var object $methods Methods for load_edit
   *
   * @access protected
   * @since 0.1
   */
  protected $methods;

  /**
   * Object constructor
   *
   * @access public
   * @since 0.2
   */
  public function __construct( $post_type ) {
    $this->post_type = $post_type;
    $this->methods = new methods\load_edit();
  }

  /**
   * Add actions to hooks
   *
   * @access public
   * @since 0.1
   */
  public function register() {
    \add_action( 'the_post', [ new hooks\the_post(), 'the_post' ], 10, 2 );
    if( \current_user_can( $this->methods->edit_capability( $this->post_type ) ) ) {
      add_action( 'load-edit.php', [ $this, 'load_edit' ], 15 );
    }
  }

  /**
   * Don't display column on future-revision post type
   *
   * @access public
   * @since 0.1
   */
  public function load_edit() {
    \add_action( 'admin_head-edit.php', [ $this, 'manage_columns' ] );
  }

  /**
   * Add filters to modify the columns
   *
   * @access public
   * @since 0.1
   */
  public function manage_columns() {
    //* Add column to show future update on the posts
    \add_filter( "manage_{$this->post_type}_posts_columns",
      [ $this->methods, 'add_column' ] );

    //* Add content to the column
    \add_filter( "manage_{$this->post_type}_posts_custom_column",
      [ $this->methods, 'custom_column' ] );
  }
}

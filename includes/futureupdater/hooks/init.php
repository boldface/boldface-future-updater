<?php
/**
 * Hooks/init
 *
 * @package Future Updater
 */
namespace Boldface\FutureUpdater\Hooks;

use \Boldface\FutureUpdater\methods as methods;
use \Boldface\FutureUpdater\hooks as hooks;

defined( 'ABSPATH' ) or die();

/**
 * Class for adding hooks to the WordPress initiation
 */
class init {

  /**
   * @var Main plugin file
   *
   * @access protected
   * @since 0.1
   */
  protected $file;

  /**
   * Object constructor
   *
   * @param string $file Main plugin file
   *
   * @access public
   * @since 0.1
   */
  public function __construct( $file ) {
    $this->file = $file;
  }

  /**
   * Add action to initialize the plugin
   *
   * @access public
   * @since 0.2
   */
  public function register() {

    //* Fires on wp_head to load hooks only for public
    \add_action( 'wp_head',
      [ new hooks\wp_head(), 'register' ], 5 );

    //* Fires on admin_init to load hooks only for admin
    \add_action( 'admin_init',
      [ new hooks\admin_init( $this->file ), 'register' ], 5 );

    //* Fires on initiation to load plugin text domain
    \add_action( 'init',
      [ new methods\load_textdomain( $this->file ), 'register' ] );

    //* Fires on initiation to register custom post type
    \add_action( 'init',
      [ new methods\custom_post_type(), 'register' ] );
  }
}
